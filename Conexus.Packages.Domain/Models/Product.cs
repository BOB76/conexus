﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Conexus.Packages.Domain.Models
{
    public class Product
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public string ProductType { get; set; }
        public Package Package { get; set; }
        public long PackageId { get; set; }
    }
}
