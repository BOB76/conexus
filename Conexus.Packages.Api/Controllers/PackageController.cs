﻿using Conexus.Packages.Domain.Contracts;
using Conexus.Packages.Domain.Models;
using Microsoft.AspNetCore.Mvc;

namespace Conexus.Packages.Api.Controllers
{
    [Route("api/[controller]")]
    public class PackageController : Controller
    {
        private readonly IPackageRepository _conexusRepository;

        public PackageController(IPackageRepository conexusRepository)
        {
            _conexusRepository = conexusRepository;
        }

        [Route("list")]
        public IActionResult List()
        {
            return Ok(_conexusRepository.GetPackagesAndProducts());
        }

        [HttpPost]
        public IActionResult Create([FromBody] Package package)
        {
            return Ok(_conexusRepository.AddPackage(package));
        }

        [HttpPut]
        public IActionResult Update([FromBody] Package package)
        {
            return Ok(_conexusRepository.UpdatePackage(package));
        }

    }
}