﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Conexus.Packages.Domain.Models
{
    public class Package
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public ICollection<Product> Products { get; set; }
    }
}
