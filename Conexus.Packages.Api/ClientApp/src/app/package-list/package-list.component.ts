import { Component, OnInit } from "@angular/core";
import { PackageService } from "../core/services/package.service";
import { ProductPackage } from "../core/models/ProductPackage";
import { MatTableDataSource } from "@angular/material";

@Component({
  selector: "app-package-list",
  templateUrl: "./package-list.component.html",
  styleUrls: ["./package-list.component.css"]
})
export class PackageListComponent implements OnInit {
  constructor(private packageService: PackageService) {}
  packages: ProductPackage[];


  ngOnInit() {
    this.getPackages();
  }

  getPackages() {
    this.packageService.getPackages().subscribe(v => {
      this.packages = v;
    });
  }
}
