﻿using Conexus.Packages.Domain.Contracts;
using Conexus.Packages.Domain.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Conexus.Packages.Infrastructure
{
    public class PackageRepository : IPackageRepository
    {
        private readonly ConexusDbContext _dbContext;

        public PackageRepository(ConexusDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public List<Package> GetPackagesAndProducts()
        {
            return _dbContext.Packages.Include(i => i.Products).ToList();
        }

        public async Task<Package> AddPackage(Package package)
        {
            await _dbContext.Packages.AddAsync(package);
            await _dbContext.SaveChangesAsync();
            return package;
        }

        public Task<Package> GetPackage(long id)
        {
            return _dbContext.Packages.Where(c => c.Id == id).Include(i => i.Products).FirstOrDefaultAsync();
        }

        public async Task<Package> UpdatePackage(Package package)
        {
            var oldPackage = await GetPackage(package.Id);
            oldPackage.Name = package.Name;

            // deleted products
            var deletedProducts = oldPackage.Products.Where(item => !package.Products.Select(c => c.Id).Contains(item.Id));
            foreach (var product in deletedProducts)
                _dbContext.Entry(product).State = EntityState.Deleted;

            // new products
            var newProducts = package.Products.Where(item => item.Id == 0);
            foreach (var product in newProducts)
            {
                _dbContext.Entry(product).State = EntityState.Added;
                oldPackage.Products.Add(product);
            }

            await _dbContext.SaveChangesAsync();
            return oldPackage;


        }
    }
}
