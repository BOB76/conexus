export class Product {
  id: number;
  name: string;
  productType: string;
  packageId: number;

  public constructor(init?: Partial<Product>) {
    Object.assign(this, init);
  }
}
