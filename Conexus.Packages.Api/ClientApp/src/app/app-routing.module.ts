import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { PackageListComponent } from "./package-list/package-list.component";
import { PackageEditComponent } from "./package-edit/package-edit.component";

const routes: Routes = [
  { path: "", redirectTo: "/packages", pathMatch: "full" },
  {
    path: "packages",
    component: PackageListComponent
  },
  {
    path: "packages/:Id",
    component: PackageEditComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
