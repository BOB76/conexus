﻿using Conexus.Packages.Domain.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace Conexus.Packages.Infrastructure
{
    public class ConexusDbContext : DbContext
    {
        public ConexusDbContext(DbContextOptions<ConexusDbContext> options)
            : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Package>()
            .Property(p => p.Id).ValueGeneratedOnAdd();

            modelBuilder.Entity<Package>()
                .HasMany(c => c.Products)
                .WithOne(e => e.Package)
                .OnDelete(DeleteBehavior.SetNull);

            modelBuilder.Entity<Product>()
                .Property(p => p.Id).ValueGeneratedOnAdd();
        }


        public DbSet<Package> Packages { get; set; }
        public DbSet<Product> Products { get; set; }


        public void GenerateSeedData()
        {
            var packs = new List<Package>
            {
                new Package { Name = "Package1"},
                new Package { Name = "Package2"},
                new Package { Name = "Package3"},
                new Package { Name = "Package4"}
            };
            Packages.AddRange(packs);

            var prods = new List<Product>
            {
                new Product { Name = "Product1", ProductType="SomeType" },
                new Product { Name = "Product2", ProductType="SomeType" },
                new Product { Name = "Product3", ProductType="SomeType" },
                new Product { Name = "Product4", ProductType="SomeType" },
                new Product { Name = "Product5", ProductType="SomeType" },
                new Product { Name = "Product6", ProductType="SomeType" },
                new Product { Name = "Product7", ProductType="SomeType" },
                new Product { Name = "Product8", ProductType="SomeType" },
                new Product { Name = "Product9", ProductType="SomeType" },
                new Product { Name = "Product10", ProductType="SomeType" }
            };
            Products.AddRange(prods);

            packs[0].Products = new List<Product>();
            packs[0].Products.Add(prods[0]);
            packs[0].Products.Add(prods[1]);
            packs[0].Products.Add(prods[2]);

            SaveChanges();
        }
    }
}
