import { Product } from "./Product";

export class ProductPackage {
  id: number;
  name: string;
  products: Product[];
}
