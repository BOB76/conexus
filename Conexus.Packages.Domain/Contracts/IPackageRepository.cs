﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Conexus.Packages.Domain.Models;

namespace Conexus.Packages.Domain.Contracts
{
    public interface IPackageRepository
    {
        Task<Package> AddPackage(Package package);
        List<Package> GetPackagesAndProducts();
        Task<Package> UpdatePackage(Package package);
    }
}