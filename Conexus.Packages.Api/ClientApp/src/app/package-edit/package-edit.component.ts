import { Component, OnInit } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { PackageService } from "../core/services/package.service";
import { ProductPackage } from "../core/models/ProductPackage";
import { Product } from "../core/models/Product";
import { FormGroup, FormControl, Validators } from "@angular/forms";

@Component({
  selector: "app-package-edit",
  templateUrl: "./package-edit.component.html",
  styleUrls: ["./package-edit.component.css"]
})
export class PackageEditComponent implements OnInit {
  package: ProductPackage;
  products: Product[];

  packageForm: FormGroup;
  addNameControl: FormControl = new FormControl(null);
  addTypeControl: FormControl = new FormControl(null);

  constructor(
    private route: ActivatedRoute,
    private packageService: PackageService,
    private router: Router
  ) {}

  ngOnInit() {
    const id = +this.route.snapshot.paramMap.get("Id");
    this.initForm();

    if (id > 0) {
      this.packageService.getPackages().subscribe(v => {
        this.package = v.find(s => s.id === id);
        this.products = this.package.products;

        this.packageForm.controls["packageTitle"].setValue(this.package.name);
      });
    } else {
      this.package = new ProductPackage();
      this.products = [];
      this.package.products = this.products;
    }
  }

  initForm() {
    this.packageForm = new FormGroup({
      addName: this.addNameControl,
      addType: this.addTypeControl,
      packageTitle: new FormControl(null, [Validators.required])
    });
  }

  removeProduct(index: number) {
    this.products.splice(index, 1);
  }

  addProduct() {
    const addType = this.addTypeControl.value;
    const addName = this.addNameControl.value;
    this.products.push(
      new Product({
        id: 0,
        packageId: this.package.id,
        name: addName,
        productType: addType
      })
    );

    this.addTypeControl.setValue("");
    this.addNameControl.setValue("");
  }

  async saveChanges() {
    this.package.name = this.packageForm.controls["packageTitle"].value;

    if (this.package.id > 0) {
      await this.packageService.updatePackage(this.package);
    } else {
      await this.packageService.createPackage(this.package);
    }

    this.router.navigate(["packages"]);
  }
}
