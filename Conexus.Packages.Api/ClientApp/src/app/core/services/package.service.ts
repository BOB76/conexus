import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { ProductPackage } from "../models/ProductPackage";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { environment } from "../../../environments/environment";

@Injectable({
  providedIn: "root"
})
export class PackageService {
  constructor(private http: HttpClient) {}

  public getPackages(): Observable<ProductPackage[]> {
    return this.http.get<ProductPackage[]>(
      `${environment.apiURL}/package/list`,
      this.getHttpOptions()
    );
  }

  createPackage(data: ProductPackage): Promise<ProductPackage> {
    return this.http
      .post<ProductPackage>(
        `${environment.apiURL}/package`,
        data,
        this.getHttpOptions()
      )
      .toPromise();
  }

  updatePackage(data: ProductPackage): Promise<ProductPackage> {
    return this.http
      .put<ProductPackage>(
        `${environment.apiURL}/package`,
        data,
        this.getHttpOptions()
      )
      .toPromise();
  }

  private getHttpOptions(): object {
    return {
      headers: new HttpHeaders({
        "Content-Type": "application/json"
      })
    };
  }
}
