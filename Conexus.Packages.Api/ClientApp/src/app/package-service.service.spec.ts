/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { PackageServiceService } from './package-service.service';

describe('Service: PackageService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [PackageServiceService]
    });
  });

  it('should ...', inject([PackageServiceService], (service: PackageServiceService) => {
    expect(service).toBeTruthy();
  }));
});
